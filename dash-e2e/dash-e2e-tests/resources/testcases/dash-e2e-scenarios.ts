import {Component} from "../../../dash-e2e-pageobjects/aspect-objects/Component";
let random = require("random-js")();

import {Navigate} from "../../../dash-e2e-pageobjects/aspect-objects/Navigate";


describe('[DASH: CREATE FLOWS]', function() {

    let dash_value :any;

    it('TEST: Create a new dashboard and verify it', function() {

        Navigate.visit('DashDashboardPage')

            .check_And_Validate_dash_page_title('Dashboards')
            .step_click_on_the_new_team_dashboard_button()
            .step_create_basic_dashboard('Dashboard - '+dash_value,'Auto Desc','R&D','VP','QA')
            .check_and_validate_dashboard_profile_title('Dashboard - '+dash_value)
            .step_move_to_landing_page_by_click_header_dashboard_tab()
            .step_enter_dash_record_under_search_box('Dashboard - '+dash_value)
            .check_and_validate_dash_record_under_table('Dashboard - '+dash_value);
    });


    beforeEach(() => {
        dash_value = random.integer(100000, 999999);
        console.log('');
        console.log('');
        console.log('');
        console.log('************************************************************************************************ ');
    });

    afterAll(() => {
        console.log('******************************** [DASH Test Automation: Suite] - COMPLETED ! ************************************');
    });


});

