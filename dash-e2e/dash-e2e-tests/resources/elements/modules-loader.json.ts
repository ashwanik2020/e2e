export default
{
    "DashHeaderModule": {
        "lnkDashLogo": {findBy: "css", value: ".nav-logo"},
        "lnkDashHome": {findBy: "css", value: ".nav-dashboards"},
        "lnkDashNotification": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[2]"},
        "lnkDashView": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[3]"},
        "lnkDashFullScreen": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[4]"},
        "lnkDashProfileIcon": {findBy: "xpath", value: "//*[@id='navbar']/ul/li[5]"},
    },
}


