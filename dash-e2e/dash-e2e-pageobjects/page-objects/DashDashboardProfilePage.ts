import {PageBase, TextField, Button, Factory, ConfigRoute} from "../class-loader";
import Elements from "../../dash-e2e-tests/resources/elements/element-loader.json";
import {DashHeaderModule} from "../module-objects/DashHeaderModule";
import {DashDashboardPage} from "./DashDashboardPage";

const log = Factory.getLogger("Page.DashDashboardProfilePage");

export class DashDashboardProfilePage extends PageBase {

    private lblDashProfileTitle: any;

    private DashHeaderModule : any

    constructor() {
        super();
        const element = Elements.DashDashboardProfilePage;
        this.DashHeaderModule = new DashHeaderModule();
        this.lblDashProfileTitle = super.findLocators(element.lblDashProfileTitle.findBy, element.lblDashProfileTitle.value);
    }

    /**
     * Validate: Verify newly crested dashboard profile page tile
     * @param title
     * @returns {DashDashboardProfilePage}
     */
    public check_and_validate_dashboard_profile_title(title: string): DashDashboardProfilePage{
        this.Helper_Assertion.expectToEqual(this.lblDashProfileTitle, title);
        log.info("Validate: Verify newly created dashboard title name [:check_and_validate_dashboard_profile_title:]");
        return new DashDashboardProfilePage();
    }

    /**
     * Step: Move back to dashboard landing page vis header dashboard tab
     * @returns {DashDashboardPage}
     */
    public step_move_to_landing_page_by_click_header_dashboard_tab(): DashDashboardPage{
        this.DashHeaderModule.action_click_on_the_dashboard_home_tab();
        log.info("Step: move back to dashboard landing page [:step_move_to_landing_page_by_click_header_dashboard_tab:]");
        return new DashDashboardPage();
    }
}